var tinycolor = require("tinycolor2");

export default function colorPalette(color:string, index:number):string {
    let hueStep = 2;
    let saturationStep = 16;
    let saturationStep2 = 5;
    let brightnessStep1 = 5;
    let brightnessStep2 = 15;
    let lightColorCount = 5;
    let darkColorCount = 4;
    let _isLight = index <= 6; 
    let _hsv = tinycolor(color).toHsv();
    let _i = _isLight ? lightColorCount + 1 - index : index - lightColorCount - 1;
  
    let getHue = function(hsv:any, i:number, isLight:boolean) {
      let hue;
      if (hsv.h >= 60 && hsv.h <= 240) {
        hue = isLight ? hsv.h - hueStep * i : hsv.h + hueStep * i;
      } else {
        hue = isLight ? hsv.h + hueStep * i : hsv.h - hueStep * i;
      }
      if (hue < 0) {
        hue += 360;
      } else if (hue >= 360) {
        hue -= 360;
      }
      return Math.round(hue);
    };
    let getSaturation = function(hsv:any, i:number, isLight:boolean) {
      let saturation;
      if (isLight) {
        saturation = Math.round(hsv.s * 100) - saturationStep * i;
      } else if (i == darkColorCount) {
        saturation = Math.round(hsv.s * 100) + saturationStep;
      } else {
        saturation = Math.round(hsv.s * 100) + saturationStep2 * i;
      }
      if (saturation > 100) {
        saturation = 100;
      }
      if (isLight && i === lightColorCount && saturation > 10) {
        saturation = 10;
      }
      if (saturation < 6) {
        saturation = 6;
      }
      return Math.round(saturation);
    };
    let getValue = function(hsv:any, i:number, isLight:boolean) {
      if (isLight) {
        return Math.round(hsv.v * 100) + brightnessStep1 * i;
      }
      return Math.round(hsv.v * 100) - brightnessStep2 * i;
    };

    return tinycolor({
        h: getHue(_hsv, _i, _isLight),
        s: getSaturation(_hsv, _i, _isLight),
        v: getValue(_hsv, _i, _isLight),
      }).toHexString();
  }