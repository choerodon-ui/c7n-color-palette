import React from 'react';
import './button.css';
import colorPalette from 'c7n-color-palette';

export interface ButtonProps {
  /**
   * What background color to use
   */
  backgroundColor?: string;
  /**
   * How large should the button be?
   */
  size?: 'small' | 'medium' | 'large';
  type?: string;
}

/**
 * Primary UI component for user interaction
 */
// @ts-ignore
export const Button: React.FC<ButtonProps> = ({
  size = 'medium',
  backgroundColor = "#3f51b5",
  type = "primery",
  ...props
}): React.ReactElement<any, any>[] => {
  const mode = 'storybook-button--primary'
  const colorNumber = 10;
  const renderItem = ():React.ReactElement<any, any>[] => {
    let items:React.ReactElement<any, any>[] = []
    for (let i = 0; i < colorNumber; i++) {
      items.push(
        <div style={{display: 'flex',flexDirection: 'column'}}>
          <div
            key={ i }
            className={['storybook-button', `storybook-button--${size}`, mode].join(' ')}
            style={{ backgroundColor: colorPalette(backgroundColor, i), minWidth:'23rem'}}
            {...props}
          >
            
            <span style={{width: '100%',textAlign: 'center',display: 'inline-block',fontWeight: 'normal',color: i<5 ? 'black':'white'}}>{ `${type}-${i}:  ${colorPalette(backgroundColor, i)}` }</span>
          </div>
        </div>
      )
    }
    return items
  }
  return (
    renderItem()
  );
};
