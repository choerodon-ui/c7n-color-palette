import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';

import { Button, ButtonProps } from './Button';

export default {
  title: 'Example/Color',
  component: Button,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<ButtonProps> = (args) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  backgroundColor: "#3f51b5",
  type: 'primary',
};

export const Purple = Template.bind({});
Purple.args = {
  backgroundColor: "#8e44ad",
  type: 'purple',
};

export const Cyan = Template.bind({});
Cyan.args = {
  backgroundColor: "#13c2c2",
  type: 'cyan',
};
export const Magenta = Template.bind({});
Magenta.args = {
  backgroundColor: "#eb2f96",
  type: 'magenta',
};
export const Pink = Template.bind({});
Pink.args = {
  backgroundColor: "#eb2f96",
  type: 'pink',
};

export const Red = Template.bind({});
Red.args = {
  backgroundColor: "#d50000",
  type: 'red',
};

export const Orange = Template.bind({});
Orange.args = {
  backgroundColor: "#fa8c16",
  type: 'orange',
};

export const Yellow = Template.bind({});
Yellow.args = {
  backgroundColor: "#fadb14",
  type: 'yellow',
};

export const Volcano = Template.bind({});
Volcano.args = {
  backgroundColor: "#fa541c",
  type: 'volcano',
};

export const Geekblue = Template.bind({});
Geekblue.args = {
  backgroundColor: "#2f54eb",
  type: 'geekblue',
};

export const Lime = Template.bind({});
Lime.args = {
  backgroundColor: "#a0d911",
  type: 'lime',
};

export const Gold = Template.bind({});
Gold.args = {
  backgroundColor: "#faad14",
  type: 'gold',
};

export const Dark = Template.bind({});
Dark.args = {
  backgroundColor: "#2f353b",
  type: 'dark',
};

